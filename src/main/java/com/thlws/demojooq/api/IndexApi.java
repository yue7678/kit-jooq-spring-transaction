package com.thlws.demojooq.api;


import com.thlws.demojooq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class IndexApi {

    @Autowired
    private UserService userService;

    @ResponseBody
    @GetMapping("/")
    public Object index(){
        return "this is index page";
    }

    /***
     * 正常插入数据
     */
    @ResponseBody
    @GetMapping("/add")
    public Object add(){

        userService.saveUser();
        return all();
    }

    /***
     * 模拟异常回滚
     */
    @ResponseBody
    @GetMapping("/add2")
    public Object add2(){
        userService.saveUser2();
        return all();
    }


    @ResponseBody
    @GetMapping("/all")
    public Object all(){
        return userService.loadUsers();
    }
}
