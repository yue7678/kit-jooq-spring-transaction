/*
 * This file is generated by jOOQ.
 */
package com.thlws.demojooq.jooq;


import com.thlws.demojooq.jooq.tables.UserTable;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in db1
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>db1.user</code>.
     */
    public static final UserTable USER = com.thlws.demojooq.jooq.tables.UserTable.USER;
}
